
<?php
define('SAVE_FEED_LOCATION','/var/www/export.csv');
set_time_limit(1800);
require_once 'app/Mage.php';
Mage::app('default');
try{
    $handle = fopen(SAVE_FEED_LOCATION, 'w');

    $heading = array(
                     '"store"',
                     '"attribute_set"',
                     '"type"',
                     '"sku"',
                     '"visibility"',
                     '"tax_class_id"',
                     '"status"',
                     '"grouped_skus"',
                     '"name"',
                     '"description"',
                     '"categories"',
                     '"image"',
                     '"small_image"',
                     '"tumbnail"',
                     '"price"',
                     '"weight"',
                     '"url_key"',
                     '"manufacturer"'
                     );
    $feed_line=implode(",", $heading)."\r\n";
    fwrite($handle, $feed_line);
    
    
    # simple products export at first... see second section for grouped products export
    $products = Mage::getModel('catalog/product')->getCollection();
    $products->addAttributeToFilter('status', 1);
    #$products->addAttributeToFilter('sku', 39664);
    #$products->addAttributeToFilter('created_at', array(
    #                                'from' => strtotime('-7 day', time()),
    #                                'to' => time(),
    #                                'datetime' => true
    #                                ));
    #$products->addAttributeToFilter('visibility', 4); 
    #$products->addAttributeToFilter('type_id', array('eq' => 'simple'));
    $products->addAttributeToSelect('*');
    $products->getSelect()->order(new Zend_Db_Expr('FIELD(e.entity_id, ' . implode(',', $productIds).')'));
    $prodIds=$products->getAllIds();

    $counter_test = 0;

    foreach($prodIds as $productId) {

        if (++$counter_test < 30000){
            
            $model = Mage::getModel('catalog/product');
            $product = $model->load($productId);
            $product_id = $product->getId();
            $product_data = array();
            
            #$product_data['id'] = $product_id;
            $product_data['store'] = "default";
            $product_data['attribute_set'] = "Default";
            $product_data['type'] = $product->getTypeId();
            $product_data['sku'] = 'mti-'.$product->getSku();
            $product_data['visibility'] = $product->getVisibility();
            $product_data['tax_class_id'] = '1';
            $product_data['status'] = $product->getStatus();

            $associated = "";
            $product_data['grouped_skus'] = $associated;
            
            $product_data['title'] = $product->getName();
            $product_data['description'] = substr(iconv("UTF-8","UTF-8//IGNORE",$product->getDescription()), 0, 900);
            
            # categories the product is in
            $cats = $product->getCategoryIds();
            
            foreach ($cats as $category_id) {
                
                $category = Mage::getModel('catalog/category')->load($category_id) ;
                $coll = $category->getResourceCollection();
                
                $pathIds = $category->getPathIds();
                $coll->addAttributeToSelect('name');
                $coll->addAttributeToFilter('entity_id', array('in' => $pathIds));
                
                # product position in category
                $positions = $category->getProductsPosition();
                
                # set all positions to 0 if no value is given
                if (!$positions[$product_id]) {
                    $position = "0";
                } else {
                    $position = $positions[$product_id];
                };
                
                # category path with category names and position for magmi import module
                #$result = '';
                foreach ($coll as $cat) {
                    $result .= $cat->getName().'/';
                }
                $result .= "::".$position.";;";
            }
            
            # replacement for root category for specifications.
            $result = str_replace("Root Catalog/Default Category/Produkte", "[Root]", "$result");
            $result = str_replace("/::", "::", "$result");
            $product_data['category'] = $result;
        
            #set images (this needs to be tested) to same value. easier should be not downloading images but use the same media folder and rsync those... BETA TESTING STATUS!!!
            $product_image = $product->getImage();
            if ($product_image == "no_selection") {
                $product_image = "";
            }
            $product_data['image'] = $product_image;
            $product_data['small_image'] = $product_image;
            $product_data['tumbnail'] = $product_image;

            # price
            $product_data['price'] = round($product->getPrice()*1.19,2);
            $product_data['weight'] = $product->getWeight();
            
            $product_data['url_key'] = $product->getUrlKey();
            $product_data['manufacturer'] = $product->getAttributeText('manufacturer');

            foreach($product_data as $k=>$val){
                $bad=array('"',"\r\n","\n","\r","\t");
                $good=array(""," "," "," ","");
                $product_data[$k] = '"'.str_replace($bad,$good,$val).'"';
            }

            echo $counter_test  . " ";

            $feed_line = implode(",", $product_data)."\r\n";
            $feed_line = str_replace(';;",', '",', $feed_line);
            fwrite($handle, $feed_line);
            fflush($handle);

        }

    }
    
    #  import grouped products in the end so all simple SKUs are there already...
    $products = Mage::getModel('catalog/product')->getCollection();
    $products->addAttributeToFilter('status', 1);
    #$products->addAttributeToFilter('created_at', array(
    #                                'from' => strtotime('-7 day', time()),
    #                                'to' => time(),
    #                                'datetime' => true
    #                                ));
    #$products->addAttributeToFilter('visibility', 4);
    $products->addAttributeToFilter('type_id', array('eq' => 'grouped'));
    $products->addAttributeToSelect('*');
    $products->getSelect()->order(new Zend_Db_Expr('FIELD(e.entity_id, ' . implode(',', $productIds).')'));
    $prodIds=$products->getAllIds();

    $counter_test = 0;

    foreach($prodIds as $productId) {

        if (++$counter_test < 30000){
            
            $model = Mage::getModel('catalog/product');
            $product = $model->load($productId);
            $product_id = $product->getId();
            $product_data = array();
            
            #$product_data['id'] = $product_id;
            $product_data['store'] = "default";
            $product_data['attribute_set'] = "Default";
            $product_data['type'] = $product->getTypeId();
            $product_data['sku'] = 'mti-'.$product->getSku();
            $product_data['visibility'] = $product->getVisibility();
            $product_data['tax_class_id'] = '';
            $product_data['status'] = $product->getStatus();

            $associated = "";
            $associatedPosition = "0";
            #get associated skus
            if ($product->getTypeId() == 'grouped'){
                $associatedProducts = $product->getTypeInstance(true)->getAssociatedProducts($product);
                
                foreach ($associatedProducts as $associatedProduct) {
                    $associated .= 'mti-'.$associatedProduct->getSku().'::'.$associatedPosition.'::0'.',';
                    $associatedPosition ++;
                }
            };
            $product_data['grouped_skus'] = $associated;
            
            $product_data['title'] = $product->getName();
            $product_data['description'] = substr(iconv("UTF-8","UTF-8//IGNORE",$product->getDescription()), 0, 900);
            
            # categories the product is in
            $cats = $product->getCategoryIds();
            foreach ($cats as $category_id) {
                
                $category = Mage::getModel('catalog/category')->load($category_id) ;
                $coll = $category->getResourceCollection();
                
                $pathIds = $category->getPathIds();
                $coll->addAttributeToSelect('name');
                $coll->addAttributeToFilter('entity_id', array('in' => $pathIds));
                
                # product position in category
                $positions = $category->getProductsPosition();
                
                #set all positions to 0 if no value is given
                if (!$positions[$product_id]) {
                    $position = "0";
                } else {
                    $position = $positions[$product_id];
                };
                
                # category path with category names and position for magmi import module
                $result = '';
                foreach ($coll as $cat) {
                    $result .= $cat->getName().'/';
                }
                $result .= "::".$position;
            }
            
            # replacement for root category for DAS specifications.
            $result = str_replace("Root Catalog/Default Category/Produkte", "[Root]", "$result");
            $result = str_replace("/::", "::", "$result");
            $product_data['category'] = $result;
            
            #set images (this needs to be tested) to same value. easier should be not downloading images but use the same media folder and rsync those... BETA TESTING STATUS!!!
            $product_image = $product->getImage();
            if ($product_image == "no_selection") {
                $product_image = "";
            }
            $product_data['image'] = $product_image;
            $product_data['small_image'] = $product_image;
            $product_data['tumbnail'] = $product_image;

            # price
            $product_data['price'] = round($product->getPrice(),2);
            $product_data['weight'] = $product->getWeight();
            
            # cable length
            $length = $product->getLength();
            if ($length == "0.00") {
                $length = "";
            }
            $product_data['length'] = $length;
            $product_data['url_key'] = $product->getUrlKey();
            $product_data['matchcode'] = $product->getMatchcode();
            $product_data['manufacturer'] = $product->getAttributeText('manufacturer');

            foreach($product_data as $k=>$val){
                $bad=array('"',"\r\n","\n","\r","\t");
                $good=array(""," "," "," ","");
                $product_data[$k] = '"'.str_replace($bad,$good,$val).'"';
            }

            echo $counter_test  . " ";

            $feed_line = implode(",", $product_data)."\r\n";
            fwrite($handle, $feed_line);
            fflush($handle);

        }

    }
    

    fclose($handle);
}
catch(Exception $e){
    die($e->getMessage());
}
?>