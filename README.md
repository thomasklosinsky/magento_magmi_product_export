# README #

Magmi Product Export Script for Magento. Tested with V 1.7.0.2 upwards.


### What is this repository for? ###

Everyone who needs an export script for MAGMI (Magento Mass Importer).

Only for simple products and grouped products.

Careful with categories. There is a replace for root category to get Magmi OnTheFly Category Importer working.

Script first export simple products then grouped products.


### How do I get set up? ###

copy export.php in Magento root folder and use cronjobs to run the script.


### Who do I talk to? ###

Thomas Klosinsky
thomas@designcomplex.de